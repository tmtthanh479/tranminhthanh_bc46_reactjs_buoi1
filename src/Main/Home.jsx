import React from "react";
import Hearder from "./Hearder";
import Banner from "./Banner";
import Item from "./Item";
import Footer from "./Footer";



const Home = () => {
  return (
    <div className="container">
      <div>
        <Hearder />
      </div>
      <div>
        <Banner />
      </div>
      <div>
        <Item/>
      </div>
      <div>
        <Footer/>
      </div>

 
    </div>
  );
};

export default Home;
