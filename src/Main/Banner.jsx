import React from "react";

const Banner = () => {
  return (
    <div>
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">A WARM WELCOME!</h5>
          <p class="card-text">
            With supporting text below as a natural lead-in to additional content.With supporting text below as a
            natural lead-in to additional content
          </p>
          <a href="#" class="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    </div>
  );
};

export default Banner;
